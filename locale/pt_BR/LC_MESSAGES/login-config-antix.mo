��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     �     �  +   �     �     �  -   �  !   (     J     \  "   j                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2024
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Autenticação Automática Alterar Alterar o Gerenciador do Início da Sessão Alterar a Imagem do Fundo Usuário Padrão Ativar o ‘Num Lock’ no Início da Sessão Gerenciador do Início da Sessão Selecionar o Tema Testar o Tema Testar o Tema Antes de Utilizá-lo 