��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     9     I     M     g     u      �     �  
   �  	   �     �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2021
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Auto-inloggning Byt Byt Inloggnings-hanterare Byt wallpaper Standard-användare Sätt på numlock vid inloggning Inloggnings-hanterare Välj Tema Test-tema Testa temat före användande 