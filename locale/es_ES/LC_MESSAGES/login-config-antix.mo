��          t      �         
             #     8     J     W     o     }  
   �     �  �  �      g     �  -   �     �     �     �  "   	     ,     =     I                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Iniciar sesión automáticamente Cambiar Cambiar el administrador de inicio de sesión Cambiar el fondo del inicio Usuario por defecto Activar Bloq Num al inicio Administrador de inicio de sesión Seleccionar tema Probar Tema Probar el tema antes de usarlo 