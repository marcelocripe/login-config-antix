��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     ?     O     U     p          �     �  	   �  	   �     �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Auto-innlogging Endre Endre innloggingsbehandler Endre bakgrunn Standardbruker Slå på NumLock ved oppstart Innloggingsbehandler Velg tema Test tema Test temaet før det tas i bruk 