��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     |     �     �     �     �  #   �               (     8                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2023
Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Automatické přihlášení Změnit Změnit způsob přihlášení Změň pozadí Výchozí uživatel Umožni numlock při přihlášení Login Manager Vyber motiv vzhledu Otestovat motiv Otestuj motiv před použitím 